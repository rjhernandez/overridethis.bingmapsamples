﻿(function() {
    "use strict";

    var list = new WinJS.Binding.List();
    var groupedItems = list.createGrouped(
        function groupKeySelector(item) { return item.group.key; },
        function groupDataSelector(item) { return item.group; }
    );

    WinJS
        .xhr({ url: "http://earthquake.usgs.gov/earthquakes/feed/v0.1/summary/1.0_day.geojson" })
        //.xhr({ url: "/data/sampleData.json"})
        .done(function (response) {

            var quakeCollection = JSON.parse(response.responseText);
            var quakes = quakeCollection.features;
            
            var waitFor = [];
            var groups = [];
            var buildGroup = function (item) {
                
                var buildGroupName = function(coord) {
                    return new WinJS.Promise(function(complete) {

                        var url = "http://dev.virtualearth.net/REST/v1/Locations/{point}?key={key}"
                            .replace("{key}", BingMaps.getKey())
                            .replace("{point}", coord.toString());

                        WinJS
                        .xhr({ url: url })
                        .done(
                            function (geocodeResponse) {
                                var geocode = JSON.parse(geocodeResponse.responseText);
                                if (geocode.resourceSets.length >= 1) {
                                    if (geocode.resourceSets[0].resources.length >= 1) {
                                        complete(geocode.resourceSets[0].resources[0].address.countryRegion);
                                    }
                                }
                                complete("Other");
                            },
                            function () {
                                complete(false);
                            });
                    });
                };
                var promise = new WinJS.Promise(function (complete) {

                    buildGroupName(item._coord)
                        .done(function(groupName) {
                            var group = groups.filter(function (g) { return g.key == groupName; })[0];
                            if (group == null) {
                                group = {
                                    key: groupName,
                                    title: groupName,
                                    subtitle: groupName,
                                    backgroundImage: null,
                                    description: groupName,
                                    _quakes: []
                                };
                                groups.push(group);
                            }
                            item.group = group;
                            group._quakes.push(item);
                            complete(true);
                        });

                });
                waitFor.push(promise);
                return promise;
            };

            var mapForQuake = function (coord) {

                var mapImage = "http://dev.virtualearth.net/REST/v1/Imagery/Map/Road/{point}/{zoom}?pp={point};1&key={key}"
                    .replace("{point}", coord.toString())
                    .replace("{point}", coord.toString())
                    .replace("{zoom}", 5)
                    .replace("{key}", BingMaps.getKey());

                return mapImage;
            };

            quakes.forEach(function (quake) {

                var item = {
                    title: quake.properties.place,
                    subtitle: quake.properties.place,
                    description: quake.properties.place,
                    content: quake.properties.place,
                    _quake: quake,
                    _coord: {
                        lat: quake.geometry.coordinates[1],
                        lng: quake.geometry.coordinates[0],
                        toString: function() {
                            return "{lat}, {lng}".replace("{lat}",
                                quake.geometry.coordinates[1]).replace("{lng}",
                                quake.geometry.coordinates[0]);
                        }
                    }
                };

                item.backgroundImage = mapForQuake(item._coord);

                buildGroup(item);
            });

            WinJS
                .Promise
                .join(waitFor)
                .done(function() {

                    var basicMap = function(lat, lng, width, height, zoom) {
                        var latLng = "{lat},{lng}"
                            .replace("{lat}", lat)
                            .replace("{lng}", lng);

                        var mapUri = "http://dev.virtualearth.net/REST/v1/Imagery/Map/Road/{coord}/{zoom}?mapSize={width},{height}&pp={coord};36;&key={key}";

                        return mapUri
                            .replace("{coord}", latLng)
                            .replace("{coord}", latLng)
                            .replace("{key}", BingMaps.getKey())
                            .replace("{width}", width)
                            .replace("{height}", height)
                            .replace("{zoom}", zoom);
                    };
                    var groupImage = function(group) {
                        return new WinJS.Promise(function (complete) {

                            var mapUri = "http://dev.virtualearth.net/REST/V1/Imagery/Map/Road/?mapLayer=Road&mapSize=480,238{pushPins}&key={key}",
                                pushPins = "",
                                pushPinIndex = 1;

                            var uri = "";

                            if (group._quakes.length == 1) {
                                var c1 = group._quakes[0]._coord;
                                group.backgroundImage = basicMap(c1.lat, c1.lng, 480, 238, 5);
                                complete(true);
                            } else {

                                var and = false;

                                group._quakes.forEach(
                                    function (quake, index) {

                                        if (index < 99) {
                                            var coord = quake._coord,
                                                pushPin = "pp={coord};36;{index}";

                                            if (and)
                                                pushPin = "&" + pushPin;

                                            pushPins = pushPins + pushPin
                                                .replace("{coord}", coord.toString())
                                                .replace("{index}", (pushPinIndex++));

                                            and = true;
                                        }

                                    });

                                uri = mapUri
                                    .replace("{pushPins}", "") // PushPins will be in body of request.
                                    .replace("{key}", BingMaps.getKey());

                                WinJS
                                    .xhr({
                                        url: uri,
                                        type: "POST",
                                        responseType: "blob",
                                        headers: {
                                            "content-type": "xxx-form-urlencoded"
                                        },
                                        data: pushPins
                                    })
                                    .done(function (result) {
                                        group.backgroundImage = URL.createObjectURL(result.response, { oneTimeOnly: false });
                                        complete(true);
                                    });
                            }


                        });
                    };

                    groups.forEach(function(group) {
                        groupImage(group)
                            .done(function() {
                                group._quakes.forEach(function(quake) {
                                    list.push(quake);
                                });
                            });
                    });

                });
        });

    WinJS.Namespace.define("Data", {
        items: groupedItems,
        groups: groupedItems.groups,
        getItemReference: getItemReference,
        getItemsFromGroup: getItemsFromGroup,
        resolveGroupReference: resolveGroupReference,
        resolveItemReference: resolveItemReference
    });

    // Get a reference for an item, using the group key and item title as a
    // unique reference to the item that can be easily serialized.
    function getItemReference(item) {
        return [item.group.key, item.title];
    }

    // This function returns a WinJS.Binding.List containing only the items
    // that belong to the provided group.
    function getItemsFromGroup(group) {
        return list.createFiltered(function (item) { return item.group.key === group.key; });
    }

    // Get the unique group corresponding to the provided group key.
    function resolveGroupReference(key) {
        for (var i = 0; i < groupedItems.groups.length; i++) {
            if (groupedItems.groups.getAt(i).key === key) {
                return groupedItems.groups.getAt(i);
            }
        }
    }

    // Get a unique item from the provided string array, which should contain a
    // group key and an item title.
    function resolveItemReference(reference) {
        for (var i = 0; i < groupedItems.length; i++) {
            var item = groupedItems.getAt(i);
            if (item.group.key === reference[0] && item.title === reference[1]) {
                return item;
            }
        }
    }

})();
