﻿(function () {
    "use strict";

    WinJS.UI.Pages.define("/pages/itemDetail/itemDetail.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {
            var item = options && options.item ? Data.resolveItemReference(options.item) : Data.items.getAt(0);
            element.querySelector(".titlearea .pagetitle").textContent = item.group.title;
            element.querySelector("article .item-title").textContent = item.title;
            element.querySelector("article .item-subtitle").textContent = item.subtitle;
            element.querySelector("article .item-image").src = item.backgroundImage;
            element.querySelector("article .item-image").alt = item.subtitle;
            element.querySelector("article .item-content").innerHTML = item.content;
            element.querySelector(".content").focus();
            
            var dataTransferManager = Windows.ApplicationModel.DataTransfer.DataTransferManager.getForCurrentView();
            dataTransferManager.addEventListener("datarequested", shareQuake.bind(item));
        }
    });
    
    function shareQuake(e) {
        var request = e.request;
        request.data.properties.title = this.group.title;
        request.data.properties.description = this.title;

        // In this example, we use the imageFile for the thumbnail as well.

        var uri = new Windows.Foundation.Uri(this.backgroundImage);
        request.data.properties.thumbnail = Windows.Storage.Streams.RandomAccessStreamReference.createFromUri(uri);
        request.data.setBitmap(Windows.Storage.Streams.RandomAccessStreamReference.createFromUri(uri));
        
        var htmlExample = "<h1>{title}</p><img src='{src}' />".replace("{title}", this.title).replace("{src}", this.backgroundImage);
        var request = e.request;
        request.data.properties.title = this.title;
        request.data.properties.description = this.title;
        var htmlFormat = Windows.ApplicationModel.DataTransfer.HtmlFormatHelper.createHtmlFormat(htmlExample);
        request.data.setHtmlFormat(htmlFormat);
    }
})();
