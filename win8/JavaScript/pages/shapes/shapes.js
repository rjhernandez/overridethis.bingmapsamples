﻿// GeoData
(function () {

    var list = new WinJS.Binding.List();

    WinJS.Namespace.define("GeoData", {
        countries: list.createSorted(function(first, second) {
            if (first.name == second.name)
                return 0;
            else if (first.name > second.name)
                return 1;
            else
                return -1;
        })
    });

    //var url = "pages/mapGeoJson/data/countries.geo.json";
    var url = "/data/gz_2010_us_040_00_20m.json";

    WinJS.xhr({ url: url })
        .done(function (raw) {

            var countries = JSON.parse(raw.response).features;

            // limit test data.
            countries.forEach(function (country) {
                list.push({
                    name: country.properties.name || country.properties.NAME,
                    _geojson: country
                });
            });
        });
})();

// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511
(function () {
    "use strict";

    WinJS.UI.Pages.define("/pages/shapes/shapes.html", {
        
        ready: function (element, options) {

            var generateMap = function(geoJson, target) {

                var buildMapShapes = function (feature) {

                    var randomColor = function () {
                        return new Microsoft.Maps.Color(Math.round(255 * Math.random()), Math.round(255 * Math.random()), Math.round(255 * Math.random()), Math.round(255 * Math.random()));
                    };
                    var shapes = [];
                    var shapeThickness = 5;
                    var shapeOptions = {
                        fillColor: randomColor(),
                        strokeColor: randomColor(),
                        strokeThickness: parseInt(shapeThickness)
                    };

                    var setOfPoints = (feature.geometry.type == "Polygon")
                        ? [feature.geometry.coordinates]
                        : feature.geometry.coordinates;

                    setOfPoints.forEach(function (points) {

                        var shapeLocations = [];

                        points[0].forEach(function (point) {

                            var lng = point[1];
                            var lat = point[0];
                            var location = new Microsoft.Maps.Location(lng, lat);

                            shapeLocations.push(location);
                        });

                        var shape = new Microsoft.Maps.Polygon(shapeLocations, shapeOptions);
                        shapes.push(shape);
                    });

                    return shapes;
                };
                var boundLocations = function (shapes) {
                    var allPoints = [];

                    shapes.forEach(function (shape) {
                        allPoints = allPoints.concat(shape.getLocations());
                    });

                    return Microsoft.Maps.LocationRect.fromLocations(allPoints);
                };

                var mapShapes = buildMapShapes(geoJson);
                var mapBounds = boundLocations(mapShapes);

                var mapOptions =
                {
                    // Add your Bing Maps key here
                    credentials: BingMaps.getKey(),
                    //bounds: mapBounds,
                    //center: new Microsoft.Maps.Location(40.71, -74.00),
                    //zoom: 8,
                    mapTypeId: Microsoft.Maps.MapTypeId.road
                };

                var mapContainer = target;
                var map = new Microsoft.Maps.Map(mapContainer, mapOptions);
                map.setView({ bounds: mapBounds });
                mapShapes.forEach(function (shape) {
                    map.entities.push(shape);
                });
            }

            var init = function () {

                var addCountryOption = function(item) {
                    var option = document.createElement("option");
                    option.text = item.name;
                    option.value = item.name;
                    countries.appendChild(option);
                };

                GeoData.countries.forEach(function (country) {
                    addCountryOption(country);
                });

                GeoData.countries.dataSource.createListBinding({
                    countChanged: function () {
                        GeoData.countries.forEach(function (country) {
                            addCountryOption(country);
                        });
                    }
                });

                GeoData.countries.addEventListener("iteminserted", function(e) {
                    var index = e.detail.index;
                    var item = GeoData.countries.getItem(index).data;
                    addCountryOption(item);
                });

                countries.addEventListener("change", function () {

                    var country = GeoData.countries
                        .filter(function (c) { return c.name === countries.value; })[0];

                    generateMap(country._geojson, document.getElementById("map"));
                });
            };
            var mapModuleLoaded = function () {
                init();
            };

            Microsoft.Maps.loadModule('Microsoft.Maps.Map', { callback: mapModuleLoaded });
        },

        unload: function () {
            // TODO: Respond to navigations away from this page.
        },

        updateLayout: function (element, viewState, lastViewState) {
            /// <param name="element" domElement="true" />

            // TODO: Respond to changes in viewState.
        }
    });
    

})();
