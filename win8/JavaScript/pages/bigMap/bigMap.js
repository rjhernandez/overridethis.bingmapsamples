﻿// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511
(function () {
    "use strict";

    WinJS.UI.Pages.define("/pages/bigMap/bigMap.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {
            // TODO: Initialize the page here.
            var init = function () {

                var mapContainer = element.querySelector("#map");
                var mapOptions = function () {
                    return {
                        credentials: BingMaps.getKey(),
                        mapTypeId: Microsoft.Maps.MapTypeId.road
                    };
                };

                var map = new Microsoft.Maps.Map(mapContainer, mapOptions());

                var locations = [];

                var addPushpin = function (item) {
                    var quake = item._quake;
                    var location = new Microsoft.Maps.Location(item._coord.lat, item._coord.lng);

                    var mag = quake.properties.mag;
                    var tier = (mag < 3) ? 1 : (mag < 5) ? 2 : 3;

                    var htmlContent = "<div class='circle-{0}'></div>".replace("{0}", tier);

                    var pushpin = new Microsoft.Maps.Pushpin(location, {
                        htmlContent: htmlContent
                    });
                    
                    locations.push(location);
                    map.entities.push(pushpin);

                    Microsoft.Maps.Events.addHandler(pushpin, 'click', function() {
                        var infoboxOptions = { title: item.title, description: "MAG: " + quake.properties.mag };
                        var defaultInfobox = new Microsoft.Maps.Infobox(location, infoboxOptions);
                        map.entities.push(defaultInfobox);
                    });

                    var mapBounds = Microsoft.Maps.LocationRect.fromLocations(locations);
                    map.setView({ bounds: mapBounds });
                };

                Data.items.addEventListener("iteminserted", function (inserted) {
                    addPushpin(inserted.detail.value);
                });
                
                Data.items.forEach(function (quake) {
                    addPushpin(quake);
                });
            };
            var mapModuleLoaded = function () {
                init.bind(this)();
            };

            Microsoft.Maps.loadModule('Microsoft.Maps.Map', { callback: mapModuleLoaded.bind(this) });
        },

        unload: function () {
            // TODO: Respond to navigations away from this page.
        },

        updateLayout: function (element, viewState, lastViewState) {
            /// <param name="element" domElement="true" />

            // TODO: Respond to changes in viewState.
        }
    });
})();
