﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Bing.Maps;
using OverrideThis.BingMapsCSharp.Common;
using OverrideThis.BingMapsCSharp.Data;
using Windows.Data.Json;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.ComponentModel;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace OverrideThis.BingMapsCSharp
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class Shapes : OverrideThis.BingMapsCSharp.Common.LayoutAwarePage
    {
        private readonly StateDataSource _stateDataSource = new StateDataSource();

        public Shapes()
        {
            this.InitializeComponent();

            this.DataContext = this;

            this._stateDataSource
                .PropertyChanged += (sender, args) =>
                {
                    if (args.PropertyName.Equals("Current", StringComparison.CurrentCultureIgnoreCase))
                    {
                        var shapeLayer = this.Map.ShapeLayers.First();
                        shapeLayer.Shapes.Clear();

                        var red = Colors.Red;
                        red.A = 127;

                        foreach (var region in _stateDataSource.Current.Regions)
                        {
                            shapeLayer
                                .Shapes
                                .Add(new MapPolygon { Locations = region.Locations, FillColor = red });
                        }

                        this.Map.SetView(new LocationRect(_stateDataSource.Current.AllLocations));
                    }
                };
        }

        #region ViewModel
        
        public class StateDataSource : BindableBase
        {

            private State _empty = new State() { Name = "-None-" }; 
            private State _current = new State();
            private ObservableCollection<State> _States = new ObservableCollection<State>();


            public StateDataSource()
            {   _current = _empty;
                _States.Add(_empty);
                Initialize();
            }

            private async void Initialize()
            {
                var raw = await GetGeoJson();
                var json = JsonObject.Parse(raw);

                foreach (var jsonState in json["features"].GetArray())
                {
                    var state = new State { Name = jsonState.GetObject()["properties"].GetObject()["NAME"].GetString() };

                    var geoRegionType = jsonState.GetObject()["geometry"].GetObject()["type"].GetString();

                    var geoRegions = (geoRegionType == "Polygon")
                        ? new [] { jsonState.GetObject()["geometry"].GetObject()["coordinates"].GetArray() }
                        : jsonState.GetObject()["geometry"].GetObject()["coordinates"].GetArray().ToArray();

                    foreach (var geoRegion in geoRegions)
                    {
                        var geoRegionPoints = geoRegion.GetArray()[0].GetArray();

                        var region = new GeoRegion();
                        
                        foreach (var geoRegionPoint in geoRegionPoints)
                        {
                            var loct = new Location()
                                           {
                                               Latitude = geoRegionPoint.GetArray()[1].GetNumber(),
                                               Longitude = geoRegionPoint.GetArray()[0].GetNumber()
                                           };
                            state.AllLocations.Add(loct);
                            region.Locations.Add(loct);
                        }

                        state.Regions.Add(region);
                    }

                    Debug.WriteLine("PROCESSED - "  + state.Name + " " + geoRegionType + " " + geoRegions.Count());


                    this._States.Add(state);
                }
            }

            private static async Task<string> GetGeoJson()
            {
                var folder = Windows.ApplicationModel.Package.Current.InstalledLocation;
                var file = await folder.GetFileAsync(@"data\gz_2010_us_040_00_20m.json");
                var content = await Windows.Storage.FileIO.ReadTextAsync(file);
                return content;
            }

            public State Current
            {
                get { return _current ?? _empty; }
                set { this.SetProperty(ref _current, value); }
            }

            public ObservableCollection<State> All
            {
                get { return _States; }
            } 
        }

        public class State : BindableBase
        {
            private string _name;
            private ObservableCollection<GeoRegion> _locations = new ObservableCollection<GeoRegion>();
            private LocationCollection _allLocations = new LocationCollection();

            public string Name
            {
                get { return _name; }
                set { this.SetProperty(ref _name, value); }
            }

            public LocationCollection AllLocations
            {
                get { return _allLocations; }
                private set { this.SetProperty(ref _allLocations, value); }
            }

            public ObservableCollection<GeoRegion> Regions
            {
                get { return _locations; }
                private set { this.SetProperty(ref _locations, value); }
            }
        }

        public class GeoRegion
        {
            public GeoRegion()
            {
                this.Locations = new LocationCollection();
            }

            public LocationCollection Locations { get; set; }
        }

        #endregion

        public string BingMapsApiKey
        {
            get
            {
                return BingMaps.ApiKey;
            }
        }

        public StateDataSource DataSource
        {
            get { return this._stateDataSource; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }
    }
}
