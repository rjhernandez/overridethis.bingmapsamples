﻿using System;
using Bing.Maps;

namespace OverrideThis.BingMapsCSharp.Data
{
    /// <summary>
    /// Generic item data model.
    /// </summary>
    public class EarthquakeDataItem : EarthquakeDataCommon
    {
        public EarthquakeDataItem(String uniqueId, String title, String subtitle, String imagePath, String description, String content, EarthquakeDataGroup group, Coordinate coordinate, double magnitude)
            : base(uniqueId, title, subtitle, imagePath, description)
        {
            this._content = content;
            this._group = group;
            this._coordinate = coordinate;
            this._magnitude = magnitude;
        }


        private string _content = string.Empty;
        public string Content
        {
            get { return this._content; }
            set { this.SetProperty(ref this._content, value); }
        }

        private EarthquakeDataGroup _group;
        public EarthquakeDataGroup Group
        {
            get { return this._group; }
            set { this.SetProperty(ref this._group, value); }
        }

        private Location _location;
        public Location Location
        {
            get { return this._location; }
            set { this.SetProperty(ref this._location, value); }
        }

        private Coordinate _coordinate;
        public Coordinate Coordinate
        {
            get { return _coordinate; }
            set { this.SetProperty(ref this._coordinate, value); }
        }

        private double _magnitude;
        public double Magnitude
        {
            get { return _magnitude; }
            set { this.SetProperty(ref this._magnitude, value); }
        }

        public int Tier()
        {
            return (this._magnitude < 3) ? 1 : (this._magnitude < 5) ? 3 : 2;
        }

    }
}