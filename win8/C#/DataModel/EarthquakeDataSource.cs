﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using Bing.Maps;
using OverrideThis.BingMapsCSharp.Common;

// The data model defined by this file serves as a representative example of a strongly-typed
// model that supports notification when members are added, removed, or modified.  The property
// names chosen coincide with data bindings in the standard item templates.
//
// Applications may use this model as a starting point and build on it, or discard it entirely and
// replace it with something appropriate to their needs.

namespace OverrideThis.BingMapsCSharp.Data
{
    /// <summary>
    /// Creates a collection of groups and items with hard-coded content.
    /// 
    /// SampleDataSource initializes with placeholder data rather than live production
    /// data so that sample data is provided at both design-time and run-time.
    /// </summary>
    public sealed class EarthquakeDataSource
    {
        private static EarthquakeDataSource _sampleDataSource = new EarthquakeDataSource();

        private ObservableCollection<EarthquakeDataGroup> _allGroups = new ObservableCollection<EarthquakeDataGroup>();
        public ObservableCollection<EarthquakeDataGroup> AllGroups
        {
            get { return this._allGroups; }
        }

        private ObservableCollection<EarthquakeDataItem> _allEarthquakes = new ObservableCollection<EarthquakeDataItem>();
        public ObservableCollection<EarthquakeDataItem> AllEarthquakes
        {

            get { return this._allEarthquakes; }
        }

        public static ObservableCollection<EarthquakeDataItem> All()
        {
            return _sampleDataSource._allEarthquakes;
        }

        public static IEnumerable<EarthquakeDataGroup> GetGroups(string uniqueId)
        {
            if (!uniqueId.Equals("AllGroups")) 
                throw new ArgumentException("Only 'AllGroups' is supported as a collection of groups");
            
            return _sampleDataSource.AllGroups;
        }

        public static EarthquakeDataGroup GetGroup(string uniqueId)
        {
            return _sampleDataSource
                .AllGroups
                .SingleOrDefault((group) => group.UniqueId.Equals(uniqueId));
        }

        public static EarthquakeDataItem GetItem(string uniqueId)
        {
            // Simple linear search is acceptable for small data sets
            var matches = _sampleDataSource.AllGroups.SelectMany(group => group.Items).Where((item) => item.UniqueId.Equals(uniqueId));
            if (matches.Count() == 1) return matches.First();
            return null;
        }

        public static async void GetAsyncEarthquakeData()
        {
            var request = new HttpClient();
            var response = await request.GetAsync("http://earthquake.usgs.gov/earthquakes/feed/v0.1/summary/1.0_day.geojson");
            var rawJson = await response.Content.ReadAsStringAsync();

            var json = Windows.Data.Json.JsonObject.Parse(rawJson);

            foreach (var jsonQuake in json["features"].GetArray())
            {
                var quake = EarthquakeDataObject.FromJson(jsonQuake);

                var group = await GetEarthquakeDataGroup(quake);
                var item = new EarthquakeDataItem(Guid.NewGuid().ToString(),
                                                  quake.Name,
                                                  "MAG: " + quake.Magnitude.ToString(),
                                                  BingMaps.MapImage(quake.Coordinate.Latitude, quake.Coordinate.Longitude),
                                                  "MAG: " + quake.Magnitude.ToString(),
                                                  "MAG: " + quake.Magnitude.ToString(),
                                                  group,
                                                  quake.Coordinate,
                                                  quake.Magnitude);
                _sampleDataSource.AllEarthquakes.Add(item);

                group.Items.Add(item);
            }
        }

        private static async Task<EarthquakeDataGroup> GetEarthquakeDataGroup(EarthquakeDataObject quake)
        {
            var groupName = await BingMaps.GetRegionName(quake.Coordinate.Latitude, quake.Coordinate.Longitude);
            var group = _sampleDataSource.AllGroups.SingleOrDefault(g => g.Title.Equals(groupName, StringComparison.CurrentCultureIgnoreCase));
            if (group == null)
            {
                group = new EarthquakeDataGroup(Guid.NewGuid().ToString(), groupName, groupName, "Assets/DarkGray.png", groupName);
                _sampleDataSource.AllGroups.Add(group);
            }
            return group;
        }

        #region  Template Code (Obsolete)
        //public EarthquakeDataSource()
        //{
        //    var ITEM_CONTENT = String.Format("Item Content: {0}\n\n{0}\n\n{0}\n\n{0}\n\n{0}\n\n{0}\n\n{0}",
        //                "Curabitur class aliquam vestibulum nam curae maecenas sed integer cras phasellus suspendisse quisque donec dis praesent accumsan bibendum pellentesque condimentum adipiscing etiam consequat vivamus dictumst aliquam duis convallis scelerisque est parturient ullamcorper aliquet fusce suspendisse nunc hac eleifend amet blandit facilisi condimentum commodo scelerisque faucibus aenean ullamcorper ante mauris dignissim consectetuer nullam lorem vestibulum habitant conubia elementum pellentesque morbi facilisis arcu sollicitudin diam cubilia aptent vestibulum auctor eget dapibus pellentesque inceptos leo egestas interdum nulla consectetuer suspendisse adipiscing pellentesque proin lobortis sollicitudin augue elit mus congue fermentum parturient fringilla euismod feugiat");

        //    var group1 = new EarthquakeDataGroup("Group-1",
        //            "Group Title: 1",
        //            "Group Subtitle: 1",
        //            "Assets/DarkGray.png",
        //            "Group Description: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempor scelerisque lorem in vehicula. Aliquam tincidunt, lacus ut sagittis tristique, turpis massa volutpat augue, eu rutrum ligula ante a ante");
        //    group1.Items.Add(new EarthquakeDataItem("Group-1-Item-1",
        //            "Item Title: 1",
        //            "Item Subtitle: 1",
        //            "Assets/LightGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group1));
        //    group1.Items.Add(new EarthquakeDataItem("Group-1-Item-2",
        //            "Item Title: 2",
        //            "Item Subtitle: 2",
        //            "Assets/DarkGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group1));
        //    group1.Items.Add(new EarthquakeDataItem("Group-1-Item-3",
        //            "Item Title: 3",
        //            "Item Subtitle: 3",
        //            "Assets/MediumGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group1));
        //    group1.Items.Add(new EarthquakeDataItem("Group-1-Item-4",
        //            "Item Title: 4",
        //            "Item Subtitle: 4",
        //            "Assets/DarkGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group1));
        //    group1.Items.Add(new EarthquakeDataItem("Group-1-Item-5",
        //            "Item Title: 5",
        //            "Item Subtitle: 5",
        //            "Assets/MediumGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group1));
        //    this.AllGroups.Add(group1);

        //    var group2 = new EarthquakeDataGroup("Group-2",
        //            "Group Title: 2",
        //            "Group Subtitle: 2",
        //            "Assets/LightGray.png",
        //            "Group Description: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempor scelerisque lorem in vehicula. Aliquam tincidunt, lacus ut sagittis tristique, turpis massa volutpat augue, eu rutrum ligula ante a ante");
        //    group2.Items.Add(new EarthquakeDataItem("Group-2-Item-1",
        //            "Item Title: 1",
        //            "Item Subtitle: 1",
        //            "Assets/DarkGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group2));
        //    group2.Items.Add(new EarthquakeDataItem("Group-2-Item-2",
        //            "Item Title: 2",
        //            "Item Subtitle: 2",
        //            "Assets/MediumGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group2));
        //    group2.Items.Add(new EarthquakeDataItem("Group-2-Item-3",
        //            "Item Title: 3",
        //            "Item Subtitle: 3",
        //            "Assets/LightGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group2));
        //    this.AllGroups.Add(group2);

        //    var group3 = new EarthquakeDataGroup("Group-3",
        //            "Group Title: 3",
        //            "Group Subtitle: 3",
        //            "Assets/MediumGray.png",
        //            "Group Description: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempor scelerisque lorem in vehicula. Aliquam tincidunt, lacus ut sagittis tristique, turpis massa volutpat augue, eu rutrum ligula ante a ante");
        //    group3.Items.Add(new EarthquakeDataItem("Group-3-Item-1",
        //            "Item Title: 1",
        //            "Item Subtitle: 1",
        //            "Assets/MediumGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group3));
        //    group3.Items.Add(new EarthquakeDataItem("Group-3-Item-2",
        //            "Item Title: 2",
        //            "Item Subtitle: 2",
        //            "Assets/LightGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group3));
        //    group3.Items.Add(new EarthquakeDataItem("Group-3-Item-3",
        //            "Item Title: 3",
        //            "Item Subtitle: 3",
        //            "Assets/DarkGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group3));
        //    group3.Items.Add(new EarthquakeDataItem("Group-3-Item-4",
        //            "Item Title: 4",
        //            "Item Subtitle: 4",
        //            "Assets/LightGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group3));
        //    group3.Items.Add(new EarthquakeDataItem("Group-3-Item-5",
        //            "Item Title: 5",
        //            "Item Subtitle: 5",
        //            "Assets/MediumGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group3));
        //    group3.Items.Add(new EarthquakeDataItem("Group-3-Item-6",
        //            "Item Title: 6",
        //            "Item Subtitle: 6",
        //            "Assets/DarkGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group3));
        //    group3.Items.Add(new EarthquakeDataItem("Group-3-Item-7",
        //            "Item Title: 7",
        //            "Item Subtitle: 7",
        //            "Assets/MediumGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group3));
        //    this.AllGroups.Add(group3);

        //    var group4 = new EarthquakeDataGroup("Group-4",
        //            "Group Title: 4",
        //            "Group Subtitle: 4",
        //            "Assets/LightGray.png",
        //            "Group Description: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempor scelerisque lorem in vehicula. Aliquam tincidunt, lacus ut sagittis tristique, turpis massa volutpat augue, eu rutrum ligula ante a ante");
        //    group4.Items.Add(new EarthquakeDataItem("Group-4-Item-1",
        //            "Item Title: 1",
        //            "Item Subtitle: 1",
        //            "Assets/DarkGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group4));
        //    group4.Items.Add(new EarthquakeDataItem("Group-4-Item-2",
        //            "Item Title: 2",
        //            "Item Subtitle: 2",
        //            "Assets/LightGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group4));
        //    group4.Items.Add(new EarthquakeDataItem("Group-4-Item-3",
        //            "Item Title: 3",
        //            "Item Subtitle: 3",
        //            "Assets/DarkGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group4));
        //    group4.Items.Add(new EarthquakeDataItem("Group-4-Item-4",
        //            "Item Title: 4",
        //            "Item Subtitle: 4",
        //            "Assets/LightGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group4));
        //    group4.Items.Add(new EarthquakeDataItem("Group-4-Item-5",
        //            "Item Title: 5",
        //            "Item Subtitle: 5",
        //            "Assets/MediumGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group4));
        //    group4.Items.Add(new EarthquakeDataItem("Group-4-Item-6",
        //            "Item Title: 6",
        //            "Item Subtitle: 6",
        //            "Assets/LightGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group4));
        //    this.AllGroups.Add(group4);

        //    var group5 = new EarthquakeDataGroup("Group-5",
        //            "Group Title: 5",
        //            "Group Subtitle: 5",
        //            "Assets/MediumGray.png",
        //            "Group Description: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempor scelerisque lorem in vehicula. Aliquam tincidunt, lacus ut sagittis tristique, turpis massa volutpat augue, eu rutrum ligula ante a ante");
        //    group5.Items.Add(new EarthquakeDataItem("Group-5-Item-1",
        //            "Item Title: 1",
        //            "Item Subtitle: 1",
        //            "Assets/LightGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group5));
        //    group5.Items.Add(new EarthquakeDataItem("Group-5-Item-2",
        //            "Item Title: 2",
        //            "Item Subtitle: 2",
        //            "Assets/DarkGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group5));
        //    group5.Items.Add(new EarthquakeDataItem("Group-5-Item-3",
        //            "Item Title: 3",
        //            "Item Subtitle: 3",
        //            "Assets/LightGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group5));
        //    group5.Items.Add(new EarthquakeDataItem("Group-5-Item-4",
        //            "Item Title: 4",
        //            "Item Subtitle: 4",
        //            "Assets/MediumGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group5));
        //    this.AllGroups.Add(group5);

        //    var group6 = new EarthquakeDataGroup("Group-6",
        //            "Group Title: 6",
        //            "Group Subtitle: 6",
        //            "Assets/DarkGray.png",
        //            "Group Description: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempor scelerisque lorem in vehicula. Aliquam tincidunt, lacus ut sagittis tristique, turpis massa volutpat augue, eu rutrum ligula ante a ante");
        //    group6.Items.Add(new EarthquakeDataItem("Group-6-Item-1",
        //            "Item Title: 1",
        //            "Item Subtitle: 1",
        //            "Assets/LightGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group6));
        //    group6.Items.Add(new EarthquakeDataItem("Group-6-Item-2",
        //            "Item Title: 2",
        //            "Item Subtitle: 2",
        //            "Assets/DarkGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group6));
        //    group6.Items.Add(new EarthquakeDataItem("Group-6-Item-3",
        //            "Item Title: 3",
        //            "Item Subtitle: 3",
        //            "Assets/MediumGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group6));
        //    group6.Items.Add(new EarthquakeDataItem("Group-6-Item-4",
        //            "Item Title: 4",
        //            "Item Subtitle: 4",
        //            "Assets/DarkGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group6));
        //    group6.Items.Add(new EarthquakeDataItem("Group-6-Item-5",
        //            "Item Title: 5",
        //            "Item Subtitle: 5",
        //            "Assets/LightGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group6));
        //    group6.Items.Add(new EarthquakeDataItem("Group-6-Item-6",
        //            "Item Title: 6",
        //            "Item Subtitle: 6",
        //            "Assets/MediumGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group6));
        //    group6.Items.Add(new EarthquakeDataItem("Group-6-Item-7",
        //            "Item Title: 7",
        //            "Item Subtitle: 7",
        //            "Assets/DarkGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group6));
        //    group6.Items.Add(new EarthquakeDataItem("Group-6-Item-8",
        //            "Item Title: 8",
        //            "Item Subtitle: 8",
        //            "Assets/LightGray.png",
        //            "Item Description: Pellentesque porta, mauris quis interdum vehicula, urna sapien ultrices velit, nec venenatis dui odio in augue. Cras posuere, enim a cursus convallis, neque turpis malesuada erat, ut adipiscing neque tortor ac erat.",
        //            ITEM_CONTENT,
        //            group6));
        //    this.AllGroups.Add(group6);
        //}
        #endregion
    }
}
