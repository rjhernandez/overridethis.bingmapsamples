﻿using Windows.Data.Json;

namespace OverrideThis.BingMapsCSharp.Data
{
    public class EarthquakeDataObject
    {
        public string Name { get; private set; }

        public double Magnitude { get; private set; }

        public Coordinate Coordinate { get; private set; }

        public static EarthquakeDataObject FromJson(IJsonValue json)
        {
            // TODO: Improve this mapping code by using Json.NET
            return new EarthquakeDataObject
                       {
                           Name = json.GetObject()["properties"].GetObject()["place"].GetString(),
                           Magnitude = json.GetObject()["properties"].GetObject()["mag"].GetNumber(),
                           Coordinate = new Coordinate
                                            {
                                                Latitude = json.GetObject()["geometry"].GetObject()["coordinates"].GetArray()[1].GetNumber(),
                                                Longitude = json.GetObject()["geometry"].GetObject()["coordinates"].GetArray()[0].GetNumber()
                                            }
                       };
        }
    }
}