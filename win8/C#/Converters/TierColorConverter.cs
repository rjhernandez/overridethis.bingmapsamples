﻿using System;
using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace OverrideThis.BingMapsCSharp.Converters
{
    public class TierColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var magnitude = (double)value;
            return new SolidColorBrush((magnitude < 3) ? Colors.Yellow : (magnitude < 5) ? Colors.Orange : Colors.Red);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotSupportedException();
        }
    }
}