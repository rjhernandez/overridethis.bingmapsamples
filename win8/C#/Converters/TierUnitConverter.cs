﻿using System;
using Bing.Maps;
using OverrideThis.BingMapsCSharp.Data;
using Windows.UI.Xaml.Data;

namespace OverrideThis.BingMapsCSharp.Converters
{
    public class TierUnitConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var magnitude = (double)value;
            return (magnitude < 3) ? 8 : (magnitude < 5) ? 10 : 15;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotSupportedException();
        }
    }
}
