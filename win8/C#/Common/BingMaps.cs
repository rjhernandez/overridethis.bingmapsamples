﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using OverrideThis.BingMapsCSharp.Data;

namespace OverrideThis.BingMapsCSharp.Common
{
    public static class BingMaps
    {
        public const string ApiKey = "AqyGO1q7O8quebtMen3S4o3aubGJHK0Ex30veuQaOz0cfuVy1ks1_k5IFmU7k7j5";

        public static string MapImage(double lat, double lng)
        {
            var coord = string.Format("{0},{1}", lat, lng);

            return string.Format("http://dev.virtualearth.net/REST/v1/Imagery/Map/Road/{0}/{1}?pp={0};1&key={2}", coord, 5, ApiKey);
        }

        public static async Task<string> GetRegionName(double lat, double lng)
        {            
            var coord = string.Format("{0},{1}", lat, lng);

            var uri = string.Format("http://dev.virtualearth.net/REST/v1/Locations/{0}?key={1}", coord, ApiKey);

            var request = new HttpClient();
            var response = await request.GetAsync(uri);
            var rawJson = await response.Content.ReadAsStringAsync();
           
            var geocodeJson = Windows.Data.Json.JsonObject.Parse(rawJson);

           
            if (geocodeJson.GetObject()["resourceSets"].GetArray().Any()) 
            {
                if (geocodeJson.GetObject()["resourceSets"].GetArray()[0].GetObject()["resources"].GetArray().Any())
                {
                    var regionName = geocodeJson.GetObject()["resourceSets"]
                            .GetArray()[0].GetObject()["resources"]
                            .GetArray()[0].GetObject()["address"]
                            .GetObject()["countryRegion"].GetString();

                    return regionName;
                }
            }
            return "Other";
        }
    }
}
